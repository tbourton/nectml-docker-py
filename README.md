# Nectml-docker-py
This package aims to provide a wrapper specified to nect service around the `docker` python cli (`pip install docker`).

## Installation
This package won't be pushed to CI or pushed to PyPI. But you can install directly from git.
```bash
pip install git+ssh://git@bitbucket.org/tbourton/nectml-docker-py.git
```

## Usage
For example, to start the face detector service:
```python
from face_detector.client import __version__ as FD_VERSION
from nectml_docker_py import nectml_docker
import requests

address = 'http://127.0.0.1:9099'
container = nectml_docker.start_service(
    service_name='facedetector',
    address=address,
    version=FD_VERSION,
    memory='2g',
    cpus=2,
)
assert requests.get(address + '/v1/health').status_code == 200
```

Note that it's also possible to run the container with access to gpu, e.g.:
```python
import requests
from face_detector.client import __version__ as FD_VERSION
from nectml_docker_py import nectml_docker

address = 'http://127.0.0.1:9099'
container = nectml_docker.start_service(
    service_name='facedetector',
    address=address,
    version=FD_VERSION,
    memory='2g',
    cpus=2,
    gpus=1,
)
assert requests.get(address + '/v1/health').status_code == 200
```
