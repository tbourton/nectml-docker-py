"""Manage docker client."""
from __future__ import annotations

import atexit
from collections.abc import Iterable
from typing import Union

import docker
import structlog

logger = structlog.get_logger()


def start_container(
    container_name: str,
    address: str,
    port: str,
    cpus: Union[int, None] = None,
    memory: Union[str, None] = None,
    gpus: Union[Iterable[int], int, None] = None,
    docker_port: str = '9000',
    print_logs: bool = False,
    name: Union[str, None] = None,
    environment: Union[dict[str, str], None] = None,
    restart_policy: Union[None, dict[str, str | int]] = None,
    auto_remove: bool = True,
) -> docker.models.containers.Container:
    """Start a docker container."""
    device_requests = []
    if gpus is not None:
        if not isinstance(gpus, Iterable):
            gpus = [gpus]
        for gpu in gpus:
            device_request = {
                'Driver': 'nvidia',
                'Capabilities': [
                    ['gpu'],
                    ['nvidia'],
                    ['compute'],
                    ['compat32'],
                    ['graphics'],
                    ['utility'],
                    ['video'],
                    ['display']
                ],  # not sure which capabilities are really needed
                'Count': gpu,
            }
            device_requests.append(
                docker.types.DeviceRequest(**device_request))

    cpus = None if not cpus else int(cpus)
    memory = None if not memory else memory
    client = docker.from_env()
    container = client.containers.run(
        container_name,
        detach=True,
        environment=environment,
        ports={docker_port: (address, port)},
        device_requests=device_requests,
        auto_remove=auto_remove,
        nano_cpus=cpus,
        mem_limit=memory,
        name=name,
        restart_policy=restart_policy,
    )
    logger.info("Started container", container_name=container_name,
                address=address, port=port)

    atexit.register(teardown_container, container, print_logs=print_logs)
    return container


def teardown_container(
    container: docker.models.containers.Container,
    print_logs: bool = False,
    write_logs: Union[str, None] = None,
) -> None:
    """Teardown the docker container."""
    try:
        logs = container.logs().decode()
        if print_logs:
            print(logs)
        if write_logs:
            with open(write_logs, 'a+') as file_:
                file_.writelines(logs)
    except Exception:  # pylint: disable=broad-except
        logger.info("Failed to process logs", exc_info=True)

    logger.info("Tearing down container", name=container.name)
    try:
        container.stop()
        container.remove()
    except docker.errors.NotFound:
        # No container to shutdown
        pass
