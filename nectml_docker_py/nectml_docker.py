"""Scipt for computing hologram detector results."""
from __future__ import annotations

import functools
import os
import pwd
import time
from collections.abc import Iterable
from typing import Union

import docker
import requests
import structlog

from .docker_cli_utils import start_container

logger = structlog.get_logger()
REGISTRY = 'harbor.int.svc.nect.com'


def start_service(
    service_name: str,
    *,
    address: str,
    version: str,
    health_route_suffix: Union[str, None] = '/v1/health',
    memory: Union[str, None] = None,
    cpus: Union[int, None] = None,
    gpus: Union[Iterable[int], int, None] = None,
    docker_port: str = '9000',
    print_logs: bool = False,
    allow_local_search: bool = False,
    restart_policy: Union[None, dict[str, str | int]] = None,
) -> docker.models.containers.Container:
    """Start nect service.

    Parameters
    ----------
    address: str
        Address to mount service at. e.g. 'http://127.0.0.1:9099'.
    version: str
        Version of the service to start. e.g. '0.8.20'.
        If allow_local_search=True, this can be a local container e.g.
        version=2548be574df4.
    service_name: str
        Name of the nect service. e.g. 'facedetector'.
    health_route_suffix: str or None
        If None, no health call will be attempted.
    memory: str or None
        Memory to allow e.g. '2g'. None means no limit.
    cpus: int or None
        Number of cpus to allow, e.g. 2. None means no limit.
    gpus: list of int, int or None
        Gpus to use. None means no gpus enabled.
    docker_port: str
        Internal docker port to use. e.g. '9010'.
    print_logs: bool
        Whether to dump logs to stdout on exit of program.
    allow_local_search: bool
        Whether to allow searching local docker if container does not exist
        in dockerreg.
    restart_policy: dict
        See https://docker-py.readthedocs.io/en/stable/containers.html for
        more info. e.g. {"Name": "on-failure", "MaximumRetryCount": 5}.

    Returns
    -------
    docker.models.containers.Container
        The created docker container.

    """
    address_, port = address.strip("http://").split(":")
    container_name = make_container_name(
        service_name, version, registry=REGISTRY)
    log = logger.bind(
        container_name=container_name,
        address=address,
        version=version,
        port=port,
        docker_address=address_,
        memory=memory,
        cpus=cpus,
        gpus=gpus,
        docker_port=docker_port,
        allow_local_search=allow_local_search,
    )

    start = functools.partial(
        start_container,
        address=address_,
        port=port,
        gpus=gpus,
        name=f"{service_name}_{get_current_user()}",
        cpus=cpus,
        memory=memory,
        print_logs=print_logs,
        docker_port=docker_port,
        environment=make_docker_env_vars(docker_port),
        restart_policy=restart_policy,
        auto_remove=(restart_policy is None),
    )

    try:
        container = start(container_name)
    except Exception:
        if allow_local_search:
            log.warning("Container not found",
                        msg="Attempting to pull locally")
            container = start(version)
        else:
            log.error("Container not found", exc_info=True)
            raise

    if health_route_suffix is not None:
        time.sleep(10)  # wait for service to start
        make_health_call(address + health_route_suffix)

    return container


def get_current_user() -> str:
    """Get current user."""
    uid = os.getuid()
    name = pwd.getpwuid(uid)[0]
    return name


def make_container_name(
    service_name: str, version: str, registry: str = REGISTRY
) -> str:
    """Craft the container name for nect services."""
    return f"{registry}/{service_name}/{service_name}:{version}"


def make_docker_env_vars(docker_port: str) -> dict[str, str]:
    """Get docker envs."""
    return {
        "HTTP_PORT": docker_port,  # old style- for back compat.
        "GUNICORN_CMD_ARGS": f"--bind=0.0.0.0:{docker_port}"
    }


def make_health_call(health_address: str) -> requests.Response:
    """Make get request to address."""
    log = logger.bind(health_address=health_address)
    try:
        health = requests.get(health_address)
    except Exception:
        log.error("Health call failed", exc_info=True)
        raise

    if not health.status_code == 200:
        log.error("Health call unsuccessful", **health.json)
        raise ConnectionError

    logger.info("Health call successful")
    return health
