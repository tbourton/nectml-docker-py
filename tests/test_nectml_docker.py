import pytest
import random
import requests

from nectml_docker_py import nectml_docker


@pytest.fixture(scope='session')
def service_name():
    """Get a mock service name."""
    return 'facedetector'


@pytest.fixture(scope='session')
def service_version():
    """Get a mock service name."""
    import face_detector.client as client
    return client.__version__


@pytest.fixture(scope='session')
def service_address():
    """Get the service address."""
    port = random.randint(9000, 9999)
    address = f'http://127.0.0.1:{port}'
    try:
        requests.get(address)
    except requests.exceptions.ConnectionError:
        return address  # If connection doesn't exist the not in use
    return service_address()  # If in use, search for new port.


@pytest.fixture(scope='session', autouse=True)
def container(service_version, service_name, service_address):
    """Get a container."""
    return nectml_docker.start_service(
        address=service_address,
        version=service_version,
        service_name=service_name,
    )


def test_health(container, service_address):
    """Test make_health_call."""
    res = nectml_docker.make_health_call(service_address + '/v1/health')
    assert res.status_code == 200


def test_container_name(container, service_name):
    """Test container is given a useful name."""
    name = container.name
    assert service_name in name
    assert nectml_docker.get_current_user() in name


def test_container_has_logs(container):
    """Test container has some logs in."""
    assert container.logs()


def test_container_status(container):
    """Test container_status."""
    assert container.status in ['running', 'created']
