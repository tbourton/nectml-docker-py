"""Setup the package.."""
import os

from setuptools import find_packages, setup

version = os.environ.get("NECTML_DOCKER_PY_VERSION", "0.0.0")

setup(
    name="nectml-docker-py",
    version=version,
    description="Docker py client utils",
    python_requires=">=3.7",
    packages=find_packages(include=["nectml_docker_py", "nectml_docker_py.*"]),
    install_requires=[
        "structlog",
        "docker",
        "requests",
    ]
)
