PYTHON_BIN ?= python3

deps:
	$(PYTHON_BIN) -m pip install ${PIP_ARGS} --index-url=https://nexus.int.svc.nect.com/repository/pypi-group/simple -r requirements-dev.txt
	$(PYTHON_BIN) -m pip install ${PIP_ARGS} --index-url=https://nexus.int.svc.nect.com/repository/pypi-group/simple -r requirements.txt && pip install ${PIP_ARGS} .

tests:
	$(PYTHON_BIN) -m pytest tests

lint:
	bandit -r nectml_docker_py
	flake8 nectml_docker_py
	isort nectml_docker_py
	pycodestyle nectml_docker_py
	pydocstyle nectml_docker_py
	pylint nectml_docker_py
	pyright nectml_docker_py

all:
	make deps
	make lint
	make tests

.PHONY: deps tests lint all
